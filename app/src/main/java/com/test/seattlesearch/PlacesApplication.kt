package com.test.seattlesearch

import android.app.Application
import com.test.seattlesearch.di.networkModule
import com.test.seattlesearch.di.repositoryModule
import com.test.seattlesearch.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class PlacesApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        initKoin()
    }

    private fun initKoin() {
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@PlacesApplication)
            modules(
                networkModule,
                repositoryModule,
                viewModelModule
            )
        }
    }
}

