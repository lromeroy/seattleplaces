package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Category(
    val icon: Icon? = null,
    val id: Int? = null,
    val name: String? = null
): Parcelable {

    fun getIconUrl() = icon?.prefix + "120" + icon?.suffix
}