package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RelatedPlaces(
    val children: List<Children>? = null,
    val parent: Parent? = null
): Parcelable