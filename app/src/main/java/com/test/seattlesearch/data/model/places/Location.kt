package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Location(
    val address: String? = null,
    @SerializedName("address_extended")
    val addressExtended: String? = null,
    val country: String? = null,
    @SerializedName("cross_street")
    val crossStreet: String? = null,
    val dma: String? = null,
    val locality: String? = null,
    val neighborhood: List<String>? = null,
    val postcode: String? = null,
    val region: String? = null
): Parcelable {

    fun getInfo() =
        "$address, $addressExtended, $locality, $region, $country"
}