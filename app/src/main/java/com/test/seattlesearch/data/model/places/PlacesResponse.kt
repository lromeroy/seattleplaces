package com.test.seattlesearch.data.model.places

import kotlinx.parcelize.Parcelize

data class PlacesResponse(
    val results: List<PlaceResult>? = null
)