package com.test.seattlesearch.data.network

import com.test.seattlesearch.data.model.photo.PlacePhoto
import com.test.seattlesearch.data.model.places.PlacesResponse
import okhttp3.OkHttpClient
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface PlacesApiService {

    /**
     * Performs the petition to fetch the results of a search
     * given the keyword, a nearLocation and a limit of items.
     * @see PlacesResponse
     * */
    @GET("search")
    suspend fun searchPlaces(
        @Query(SEARCH_QUERY) keyword: String,
        @Query(NEAR_QUERY) nearLocation: String,
        @Query(LIMIT_QUERY) limit: Int
    ): PlacesResponse

    /**
     * Performs the petition to fetch the photos of a places given it's id.
     * @see PlacePhoto
     * */
    @GET("{$PATH_ID}/photos")
    suspend fun getPlacePhotos(
        @Path(PATH_ID) placeId: String,
        @Query(LIMIT_QUERY) limit: Int
    ): List<PlacePhoto>

    companion object {

        private const val PATH_ID = "id"
        private const val SEARCH_QUERY = "query"
        private const val NEAR_QUERY = "near"
        private const val LIMIT_QUERY = "limit"

        fun create(url: String, client: OkHttpClient, converterFactory: Converter.Factory): PlacesApiService =
            Retrofit.Builder()
                .client(client)
                .addConverterFactory(converterFactory)
                .baseUrl(url)
                .build()
                .create(PlacesApiService::class.java)

    }
}