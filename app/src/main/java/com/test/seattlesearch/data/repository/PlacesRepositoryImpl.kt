package com.test.seattlesearch.data.repository

import com.test.seattlesearch.data.model.places.PlaceResult
import com.test.seattlesearch.data.network.PlacesApiService
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext

class PlacesRepositoryImpl(
    private val placesApiService: PlacesApiService,
    private val dispatcher: CoroutineDispatcher
) : PlacesRepository {

    /**
     * Fetches the places related to a keyword.
     * @see PlacesApiService.searchPlaces
     * */
    override suspend fun searchPlaces(keyword: String): List<PlaceResult>? =
        withContext(dispatcher) {
            val response = placesApiService.searchPlaces(
                keyword,
                SEATTLE_LOCATION,
                RESULT_LIMIT
            )
            response.results
        }

    /**
     * Fetches the place's photos given it's id and returns a list of the photos urls
     * @see PlacesApiService.getPlacePhotos
     * */
    override suspend fun getPlacePhotos(placeId: String): List<String>? = withContext(dispatcher) {
        val response = placesApiService.getPlacePhotos(placeId, 5)
        val photosUrlList = mutableListOf<String>().apply {
            response.forEach {
                add(it.prefix + PHOTO_BUILD_STRING + it.suffix)
            }
        }
        photosUrlList
    }

    companion object {
        const val SEATTLE_LOCATION = "Seattle,+WA"
        const val RESULT_LIMIT = 20
        const val PHOTO_BUILD_STRING = "original"
    }
}