package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class Children(
    @SerializedName("fsq_id")
    val itemId: String? = null,
    val name: String? = null
): Parcelable