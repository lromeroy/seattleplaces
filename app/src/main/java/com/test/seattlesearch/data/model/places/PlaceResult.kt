package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlaceResult(
    val categories: List<Category>? = null,
    val distance: Int? = null,
    @SerializedName("fsq_id")
    val placeId: String? = null,
    val geocodes: Geocodes? = null,
    val location: Location? = null,
    val name: String? = null,
    @SerializedName("related_places")
    val relatedPlaces: RelatedPlaces? = null,
    val timezone: String? = null
) : Parcelable