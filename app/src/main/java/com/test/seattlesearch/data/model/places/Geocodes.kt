package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Geocodes(
    val main: Main? = null
): Parcelable