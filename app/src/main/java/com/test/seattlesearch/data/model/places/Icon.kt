package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Icon(
    val prefix: String? = null,
    val suffix: String? = null
): Parcelable