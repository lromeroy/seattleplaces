package com.test.seattlesearch.data.network

import com.test.seattlesearch.BuildConfig
import com.test.seattlesearch.data.network.interceptor.AuthorizationInterceptor
import com.test.seattlesearch.data.network.interceptor.ConnectivityInterceptor
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object HttClientManager {

    fun getOkHttpClient(connectivityInterceptor: ConnectivityInterceptor? = null,
                        authorizationInterceptor: AuthorizationInterceptor? = null): OkHttpClient {
        val client = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            client.addInterceptor(loggingInterceptor)
        }

        client.connectTimeout(60, TimeUnit.SECONDS)
        client.readTimeout(60, TimeUnit.SECONDS)
        client.writeTimeout(60, TimeUnit.SECONDS)
        connectivityInterceptor?.let { interceptor -> client.addInterceptor(interceptor) }
        authorizationInterceptor?.let { interceptor -> client.addInterceptor(interceptor) }
        return client.build()
    }
}