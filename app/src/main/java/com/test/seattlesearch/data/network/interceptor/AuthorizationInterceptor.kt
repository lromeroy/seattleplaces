package com.test.seattlesearch.data.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class AuthorizationInterceptor(private val authorization: String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url.newBuilder()
            .addQueryParameter(FORMAT, JSON_FORMAT)
            .build()

        val newRequest = if (authorization.isNotEmpty())
            chain.request()
                .newBuilder()
                .url(url)
                .addHeader(HEADER_AUTHORIZATION, authorization)
                .build() else chain.request()
            .newBuilder()
            .url(url)
            .build()

        return chain.proceed(newRequest)
    }

    companion object {
        const val HEADER_AUTHORIZATION = "Authorization"
        const val FORMAT = "format"
        const val JSON_FORMAT = "JSON"
    }
}