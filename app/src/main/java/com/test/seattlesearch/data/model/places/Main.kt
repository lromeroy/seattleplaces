package com.test.seattlesearch.data.model.places

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Main(
    val latitude: Double? = null,
    val longitude: Double? = null
): Parcelable