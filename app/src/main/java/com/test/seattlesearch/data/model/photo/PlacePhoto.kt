package com.test.seattlesearch.data.model.photo

import com.google.gson.annotations.SerializedName

data class PlacePhoto(
    val classifications: List<String>? = null,
    @SerializedName("created_at")
    val createdAt: String? = null,
    val height: Int? = null,
    val id: String? = null,
    val prefix: String? = null,
    val suffix: String? = null,
    val width: Int? = null
)