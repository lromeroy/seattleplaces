package com.test.seattlesearch.data.repository

import com.test.seattlesearch.data.model.places.PlaceResult

interface PlacesRepository {

    /**
     * Fetches the places related to a keyword.
     * @see
     * */
    suspend fun searchPlaces(keyword: String): List<PlaceResult>?
    suspend fun getPlacePhotos(placeId: String): List<String>?
}