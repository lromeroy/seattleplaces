package com.test.seattlesearch.di

import com.test.seattlesearch.BuildConfig
import com.test.seattlesearch.R
import com.test.seattlesearch.data.network.HttClientManager
import com.test.seattlesearch.data.network.PlacesApiService
import com.test.seattlesearch.data.network.interceptor.AuthorizationInterceptor
import com.test.seattlesearch.data.network.interceptor.ConnectivityInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory

private const val PLACES_URL = "PLACES_URL"
private const val PLACES_API_KEY = "PLACES_API_KEY"

/**
 * Contains network related objets for dependency injection across the project
 * */
val networkModule = module {

    single(named(PLACES_URL)) { BuildConfig.PLACES_URL }
    single(named(PLACES_API_KEY)) { androidContext().applicationContext.getString(R.string.foursquare_api_key)}

    single<Converter.Factory> { GsonConverterFactory.create() }
    single { ConnectivityInterceptor(androidContext()) }
    single { AuthorizationInterceptor(get(named(PLACES_API_KEY))) }
    factory { HttClientManager.getOkHttpClient(get(), get()) }
    single { PlacesApiService.create(get(named(PLACES_URL)), get(), get()) }
}