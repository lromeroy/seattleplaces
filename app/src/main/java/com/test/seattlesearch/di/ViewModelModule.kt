package com.test.seattlesearch.di

import com.test.seattlesearch.ui.viewmodel.PlaceDetailViewModel
import com.test.seattlesearch.ui.viewmodel.PlacesViewModel
import org.koin.androidx.viewmodel.dsl.viewModel

import org.koin.dsl.module

/**
 * Contains the viewModel to be injected on UI classes
 * */
val viewModelModule = module {
    viewModel { PlacesViewModel(get()) }
    viewModel { PlaceDetailViewModel(get()) }
}