package com.test.seattlesearch.di

import com.test.seattlesearch.data.repository.PlacesRepository
import com.test.seattlesearch.data.repository.PlacesRepositoryImpl
import kotlinx.coroutines.Dispatchers
import org.koin.core.qualifier.named
import org.koin.dsl.module

/**
 * Contains repository related objets for dependency injection across the project
 * */
val repositoryModule = module {

    single { Dispatchers.IO }
    factory<PlacesRepository> { PlacesRepositoryImpl(get(), get()) }
}