package com.test.seattlesearch.common

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule


@GlideModule
class PlacesAppGlideModule : AppGlideModule()