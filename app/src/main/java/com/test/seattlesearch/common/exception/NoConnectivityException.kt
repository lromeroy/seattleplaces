package com.test.seattlesearch.common.exception

import java.io.IOException

class NoConnectivityException : IOException()