package com.test.seattlesearch.common

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.core.KoinComponent
import kotlin.coroutines.CoroutineContext

open class BaseViewModel: ViewModel(), CoroutineScope, KoinComponent {

    val loadingState = MutableLiveData<Boolean>()
    val onError = MutableLiveData<String>()
    var job = Job()

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private fun cancelJob() {
        if (job.isActive) job.cancel()
    }

    fun resetJob() {
        cancelJob()
        job = Job()
    }

    protected fun showDefaultError(throwable: Throwable? = null) {
        onError.postValue("An error has ocurred, please try again later.")
        throwable?.let {
            Log.d("Error" + this.javaClass.canonicalName, it.message.orEmpty(), throwable)
        }
    }

    protected fun showConnectionError(throwable: Throwable? = null) {
        onError.postValue("No internet connection.")
        throwable?.let {
            Log.d(this.javaClass.canonicalName, it.message.orEmpty(), throwable)
        }
    }
}