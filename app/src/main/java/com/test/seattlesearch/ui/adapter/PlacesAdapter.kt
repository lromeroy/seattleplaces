package com.test.seattlesearch.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.test.seattlesearch.BR
import com.test.seattlesearch.common.loadUrl
import com.test.seattlesearch.data.model.places.PlaceResult
import com.test.seattlesearch.databinding.ItemPlaceBinding
import com.test.seattlesearch.ui.adapter.callback.PlacesCallback
import org.koin.core.KoinComponent

class PlacesAdapter(private val onItemClick: (PlaceResult) -> Unit) :
    RecyclerView.Adapter<PlacesAdapter.PlacesViewHolder>() {

    private val places: MutableList<PlaceResult> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlacesViewHolder {
        val itemBinding =
            ItemPlaceBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return PlacesViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: PlacesViewHolder, position: Int) {
        val place = places[position]
        holder.bind(place, onItemClick)
    }

    override fun getItemCount(): Int = places.size

    fun setData(list: List<PlaceResult>) {
        val diffCallback = PlacesCallback(places, list)
        val diff = DiffUtil.calculateDiff(diffCallback)
        places.clear()
        places.addAll(list)
        diff.dispatchUpdatesTo(this)
    }

    class PlacesViewHolder(private val binding: ItemPlaceBinding) :
        RecyclerView.ViewHolder(binding.root), KoinComponent {

        fun bind(place: PlaceResult, onItemClick: (PlaceResult) -> Unit) {
            binding.loading = true
            binding.setVariable(BR.place, place)
            binding.ivPlace.loadUrl(place.categories?.first()?.getIconUrl().orEmpty(), true) {
                binding.loading = false
            }
            binding.root.setOnClickListener {
                onItemClick(place)
            }
        }
    }

}
