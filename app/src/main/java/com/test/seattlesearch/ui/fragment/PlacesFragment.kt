package com.test.seattlesearch.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.test.seattlesearch.common.afterTextChanged
import com.test.seattlesearch.common.shortToast
import com.test.seattlesearch.databinding.FragmentPlacesBinding
import com.test.seattlesearch.ui.viewmodel.PlacesViewModel
import org.koin.android.ext.android.inject

/*
 * Allows a user to search places from keywords ex: "coffee", "yogurt". and shows a list
 * of results, every item when clicked navigates to it's detail.
 */

class PlacesFragment : Fragment() {

    private val viewModel: PlacesViewModel by inject()
    private var _binding: FragmentPlacesBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPlacesBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.rvPlaces.adapter = viewModel.adapter
        binding.viewModel = viewModel
        observeViewModel()
    }

    private fun observeViewModel() = viewModel.run {
        onError.observe(viewLifecycleOwner, {
            shortToast(it)
        })
        loadingState.observe(viewLifecycleOwner, { loading ->
            binding.loading = loading
        })
        onItemClick.observe(viewLifecycleOwner, { place ->
            findNavController().navigate(PlacesFragmentDirections.actionPlacesFragmentToPlaceDetailFragment(place))
        })
        setTextListener.observe(viewLifecycleOwner, {
            setupSearch()
        })
    }

    private fun setupSearch() {
        binding.etPlacesSearch.afterTextChanged { text ->
            viewModel.searchPlaces(text)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}