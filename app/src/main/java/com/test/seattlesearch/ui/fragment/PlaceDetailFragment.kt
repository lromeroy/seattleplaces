package com.test.seattlesearch.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.test.seattlesearch.common.makeGone
import com.test.seattlesearch.common.shortToast
import com.test.seattlesearch.databinding.FragmentPlaceDetailBinding
import com.test.seattlesearch.ui.adapter.CategoryAdapter
import com.test.seattlesearch.ui.adapter.ImagePagerAdapter
import com.test.seattlesearch.ui.viewmodel.PlaceDetailViewModel
import org.koin.core.KoinComponent
import org.koin.core.inject

/*
 * Shows the selected places detail, containing images, name, distance from Seattle center, categories,
 *  and the address location of it.
 */

class PlaceDetailFragment : Fragment(), KoinComponent {

    private var _binding: FragmentPlaceDetailBinding? = null
    private val binding get() = _binding!!
    private val viewModel: PlaceDetailViewModel by inject()
    private val arguments: PlaceDetailFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPlaceDetailBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.apply {
            onError.observe(viewLifecycleOwner) {
                shortToast(it)
            }
            loadingState.observe(viewLifecycleOwner, { loading ->
                binding.loading = loading
            })
            photosList.observe(viewLifecycleOwner, {
                setPlacePhotos(it)
            })
        }
        setPlaceInfo()
    }

    private fun setPlacePhotos(photosList: List<String>?) {
        photosList?.let {
            binding.tabLayout.setupWithViewPager(binding.viewPager)
            viewModel.viewPagerAdapter = ImagePagerAdapter(requireContext(), it)
            binding.viewPager.adapter = viewModel.viewPagerAdapter
        }
    }

    private fun setPlaceInfo() {
        arguments.placeResult?.apply {
            binding.place = this
            viewModel.getPlacePhotos(placeId.orEmpty())
            this.categories?.let {
                viewModel.categoriesAdapter = CategoryAdapter(it)
                binding.rvCategories.adapter = viewModel.categoriesAdapter
                binding.rvCategories.layoutManager =
                    LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
            } ?: run {
                binding.rvCategories.makeGone()
                binding.tvCategories.makeGone()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}