package com.test.seattlesearch.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import com.test.seattlesearch.common.BaseViewModel
import com.test.seattlesearch.common.exception.NoConnectivityException
import com.test.seattlesearch.data.repository.PlacesRepository
import com.test.seattlesearch.ui.adapter.CategoryAdapter
import com.test.seattlesearch.ui.adapter.ImagePagerAdapter
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch

/*
 * ViewModel that handles data delivery for the detail of a place and contains data relevant
 * to maintain the lifecycle of given objects. Acts as bridge
 * for the viewModel layer between the UI classes and the repository.
 */

class PlaceDetailViewModel(private val placesRepository: PlacesRepository): BaseViewModel() {

    var viewPagerAdapter: ImagePagerAdapter? = null
    var categoriesAdapter: CategoryAdapter? = null
    var photosList = MutableLiveData<List<String>>()


    /**
     * Receives a placeId and returns a list of its photo's url .
     * @see PlacesRepository.getPlacePhotos
     */
    fun getPlacePhotos(placeId: String) = launch {
        loadingState.postValue(true)
        val result = runCatching { placesRepository.getPlacePhotos(placeId) }
        loadingState.postValue(false)

        result.onSuccess {
            photosList.postValue(it)
        }.onFailure { e ->
            if (e is NoConnectivityException)
                showConnectionError(e)
            else if (e !is CancellationException)
                showDefaultError(e)
        }
    }
}