package com.test.seattlesearch.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.test.seattlesearch.BR
import com.test.seattlesearch.common.loadUrl
import com.test.seattlesearch.data.model.places.Category
import com.test.seattlesearch.databinding.ItemCategoryBinding
import org.koin.core.KoinComponent

class CategoryAdapter(private val categories: List<Category>) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val itemBinding =
            ItemCategoryBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return CategoryViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val category = categories[position]
        holder.bind(category)
    }

    override fun getItemCount(): Int = categories.size


    class CategoryViewHolder(private val binding: ItemCategoryBinding) :
        RecyclerView.ViewHolder(binding.root), KoinComponent {

        fun bind(category: Category) {
            binding.loading = true
            binding.setVariable(BR.category, category)
            binding.ivCategory.loadUrl(category.getIconUrl(), true) {
                binding.loading = false
            }
        }
    }

}