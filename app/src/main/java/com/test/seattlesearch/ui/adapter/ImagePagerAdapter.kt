package com.test.seattlesearch.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.test.seattlesearch.common.loadUrl
import com.test.seattlesearch.databinding.ItemImageBinding

class ImagePagerAdapter(val context: Context, private val images: List<String>) :
    PagerAdapter() {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getCount() = images.size

    override fun isViewFromObject(view: View, p1: Any) = view === p1 as View

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val binding =
            ItemImageBinding.inflate(layoutInflater, container, false)
        setContentData(position, binding)
        container.addView(binding.root)
        return binding.root
    }

    private fun setContentData(position: Int, binding: ItemImageBinding) {
        binding.loading = true
        binding.imageView.loadUrl(images[position]) {
            binding.loading = false
        }
    }

    override fun destroyItem(container: ViewGroup, position: Int, p1: Any) {
        container.removeView(p1 as View)
    }
}