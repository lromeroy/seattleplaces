package com.test.seattlesearch.ui.adapter.callback

import androidx.recyclerview.widget.DiffUtil
import com.test.seattlesearch.data.model.places.PlaceResult

class PlacesCallback(
    private val oldList: List<PlaceResult>,
    private val newList: List<PlaceResult>
) : DiffUtil.Callback() {

    override fun getOldListSize(): Int = oldList.size
    override fun getNewListSize(): Int = newList.size

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        return oldList[oldItemPosition] === newList[newItemPosition]
    }

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean {
        val oldPlace = oldList[oldPosition]
        val newPlace = newList[newPosition]
        return oldPlace.name == newPlace.name && oldPlace.placeId == newPlace.placeId
    }
}