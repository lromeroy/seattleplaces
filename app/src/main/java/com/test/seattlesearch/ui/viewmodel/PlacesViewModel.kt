package com.test.seattlesearch.ui.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.test.seattlesearch.common.BaseViewModel
import com.test.seattlesearch.common.SingleLiveEvent
import com.test.seattlesearch.common.exception.NoConnectivityException
import com.test.seattlesearch.data.model.places.PlaceResult
import com.test.seattlesearch.data.repository.PlacesRepository
import com.test.seattlesearch.ui.adapter.PlacesAdapter
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.launch

/*
 * ViewModel that handles data delivery for the places search and contains data relevant
 * to maintain the lifecycle of given objects. Acts as bridge
 * for the viewModel layer between the UI classes and the repository.
 */
class PlacesViewModel(private val placesRepository: PlacesRepository) : BaseViewModel() {

    val adapter = PlacesAdapter { place ->
        onItemClick.postValue(place)
    }
    val showNoResult : ObservableField<Boolean> = ObservableField(false)
    val showNoConnection : ObservableField<Boolean> = ObservableField(false)
    val showResults : ObservableField<Boolean> = ObservableField(false)
    val onItemClick = SingleLiveEvent<PlaceResult>()
    val setTextListener = MutableLiveData<Unit>()

    init {
        setTextListener.postValue(Unit)
    }

    /**
     * Validates the length of the keyword to fetch the places or if it's empty to clear the list
     */
    fun searchPlaces(keyword: String) = launch {
        showNoConnection.set(false)
        if (keyword.length >= 3)
            fetchPlaces(keyword)
        else if (keyword.isEmpty()) {
            clearList()
        }
    }


    /**
     * Receives a keyword to search and fetches the results of it.
     * If a petition is in progress already, it's canceled to prevent concurrent search.
     * @see PlaceResult
     * @see PlacesRepository.searchPlaces
     */
    private fun fetchPlaces(keyword: String) {
        if (job.isActive || !job.isCompleted) resetJob()
        launch {
            loadingState.postValue(true)
            val result = runCatching { placesRepository.searchPlaces(keyword) }

            result.onSuccess { places ->
                loadingState.postValue(false)
                places?.let {
                    showNoResult.set(places.isEmpty())
                    showResults.set(places.isNotEmpty())
                    adapter.setData(places)
                }
            }.onFailure { e ->
                if (e is NoConnectivityException) {
                    showNoConnection.set(true)
                    loadingState.postValue(false)
                    showConnectionError(e)
                } else if (e !is CancellationException) {
                    loadingState.postValue(false)
                    showDefaultError(e)
                }
            }
        }
    }

    /**
     * Clears the places list.
     * If a petition is in progress already, it's canceled.
     * Resets the observable values to clear the view.
     */
    private fun clearList() {
        resetJob()
        adapter.setData(listOf())
        loadingState.postValue(false)
        showNoResult.set(false)
        showNoConnection.set(false)
        showResults.set(false)
    }
}