# Seattle Places
Search locations near Seattle.

## Project Structure
MVVM based project.

### Folder
- **common**: contains classes and functions used across the whole project.
- **data**: contains classes related to manage the fetching of data and it's configuration.
- **di**: contains the divided modules to manage the dependency injection across the project.
- **ui**: contains UI related files, Activities, Fragments, ViewModels and all classes that involve view communication and configuration.

### Major Libraries
- [Koin](https://insert-koin.io/docs/quickstart/android): for dependency injection.
- [Navigation Component](https://developer.android.com/guide/navigation/navigation-getting-started): for view navigation .
- [DataBinding](https://developer.android.com/topic/libraries/data-binding): for passing data to xml.
- [ViewBinding](https://developer.android.com/topic/libraries/view-binding): for relating xml to UI classes.
- [Retrofit](https://square.github.io/retrofit/): for handling API Requests.
- [Foursquare Places API](https://developer.foursquare.com/reference/places-nearby): for fetching the places.

### Requirements
- Make sure you have **Java 11** on your Gradle JDK (Go to -> File -> Settings -> Build, Execution, Deployment -> Build Tools -> Gradle)